class Array
   def odds
      self.select { |i| i.odd? }
   end

   def evens
      self.select { |i| i.even? }
   end
end

def get_abundant_numbers
   (12..20161).select do |i|
      sum = 1
      sqrt = Math.sqrt(i).to_i
      if(sqrt ** 2 === i)
         sum += sqrt
         sqrt -= 1
      end
      sum += (2..sqrt).select { |j| i % j == 0 }.collect { |k| k + i / k }.inject(:+).to_i
      sum > i
   end
end

def get_nonabundant_sums(abundant_numbers)
   e_an = abundant_numbers.evens
   o_an = abundant_numbers.odds
   num_hist = []
   sum = 0
   (1..20161).select do |i|
      is_abundant = false
      an = i % 2 == 0 ? e_an : o_an
      an.each do |j|
         diff = i - j
         num_hist[j] = 1
         if diff > 0 && num_hist[diff]
            is_abundant = true
            break
         end
      end
      !is_abundant
   end
end
abundant_numbers = get_abundant_numbers
nas = get_nonabundant_sums abundant_numbers
p nas.inject(:+)

